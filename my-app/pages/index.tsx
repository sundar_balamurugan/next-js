import type { NextPage } from 'next'
import SampleForm from './components/Form'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

const Home: NextPage = () => {
  return (
    <div >
      <SampleForm />
    </div>
  )
}

export default Home;
