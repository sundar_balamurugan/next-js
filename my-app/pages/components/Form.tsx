import React from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap'
import { forminterface } from '../Interfaes/interface';

class SampleForm extends React.Component<{}, forminterface> {
    state: forminterface = {
        email: '',
        password: '',
        address1: '',
        address2: '',
        city: '',
        zip: '',
    }

    handleChange = (e: { target: { name: any; value: any }; }) => {
        const { name, value } = e.target
        this.setState({ [name]: value } as Pick<forminterface, keyof forminterface>)
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        const FormData = {
            email: this.state.email,
            password: this.state.password,
            address1: this.state.address1,
            address2: this.state.address2,
            city: this.state.city,
            zip: this.state.zip
        }
    }

    render() {
        return (
            <div >
                <Container>
                    <h1>Sample Form</h1>
                    <Form>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Label>Email</Form.Label>
                                <Form.Control type="email" placeholder="Enter email"
                                    name='email' value={this.state.email}
                                    onChange={this.handleChange}
                                />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password"
                                    name='password' value={this.state.password}
                                    onChange={this.handleChange} />
                            </Form.Group>
                        </Row>

                        <Form.Group className="mb-3" controlId="formGridAddress1">
                            <Form.Label>Address</Form.Label>
                            <Form.Control placeholder="1234 Main St"
                                name='address1' value={this.state.address1}
                                onChange={this.handleChange} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formGridAddress2">
                            <Form.Label>Address 2</Form.Label>
                            <Form.Control placeholder="Apartment, studio, or floor"
                                name='address2' value={this.state.address2}
                                onChange={this.handleChange} />
                        </Form.Group>

                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="formGridCity">
                                <Form.Label>City</Form.Label>
                                <Form.Control
                                    name='city' value={this.state.city}
                                    onChange={this.handleChange} />
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridZip">
                                <Form.Label>Zip</Form.Label>
                                <Form.Control
                                    name='zip' value={this.state.zip}
                                    onChange={this.handleChange} />
                            </Form.Group>
                        </Row>

                        <Form.Group className="mb-3" id="formGridCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group>

                        <Button variant="primary" type="submit" onClick={(e) => this.handleSubmit(e)}>
                            Submit
                        </Button>
                    </Form>
                </Container>
            </div>
        )
    }
}

export default SampleForm;