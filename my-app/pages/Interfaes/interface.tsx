import React from 'react';

interface mystate {
    email: string,
    password: string,
    address1: string,
    address2: string,
    city: string,
    zip: string
}

export type forminterface = mystate;